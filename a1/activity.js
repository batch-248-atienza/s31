//a. What directive is used by Node.js in loading the modules it needs?
	//requires(moduleName)

//b. What Node.js module contains a method for server creation?
	//http module

//c. What is the method of the http object responsible for creating a server using Node.js?
	//http.createServer()

//d. What method of the response object allows us to set status codes and content types?
	//response.writeHead()

//e. Where will console.log() output its contents when run in Node.js?
	//will output in the terminal (in this case, Git Bash)

//f. What property of the request object contains the address' endpoint?
	//request.url
