const http = require('http');
const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url == '/login'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to the login page.');
	} else {
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end('This page does not exist. Perhaps you were looking for the login page?');
	};
});

server.listen(port);
console.log(`Server is now accessible at localhost:${port}`);